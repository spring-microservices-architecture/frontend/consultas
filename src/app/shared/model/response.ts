export class Response<T extends any> {
    timestamp: Date;
    message: string;
    details: string;
    obj: T;
}