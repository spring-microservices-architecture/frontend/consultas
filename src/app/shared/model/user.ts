export class User {
    constructor(public idUsuario:number,
        public idMunicipalidad: number,
        public nombres:string,
        public paterno: string,
        public materno: string,
        public dni: string,
        public username: string,
        public password: string,
        public correo: string,
        public celular: number,
        public estado: number,
        public passwordChanged: number,
        public ipAddress?: string,
        public authorities?: Array<any>,
        public usuario?: string,
        public clave?: string) {
    }
}