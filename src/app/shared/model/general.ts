export class General {
    constructor(
        public municipalidad: string,
        public ubigeo: string,
        public ruc: string,
        public dolphin: boolean,
        public hytera: boolean,
        public idSectorista?: number,
    ){}
}