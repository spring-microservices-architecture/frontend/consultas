import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
    providedIn: 'root'
})
export class SweetAlertService {
    constructor() { }

    validate(value?: string) {
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: value || 'Operación no válida',
            footer: ''
        })
    }

    confirmAndDeleteWithValue(value?: string) {
        return Swal.fire({
            title: '¿Estas seguro de eliminar ' + (value === undefined ? 'este registro' : 'los datos de ' + value) + '?',
            text: "Esta acción no se podra revertir.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '¡Sí, Eliminalo!',
            cancelButtonText: 'No, cancelalo',
        });
    }

    deleted() {
        return Swal.fire('¡Eliminado!',
            'El registro ha sido eliminado.',
            'success');
    }

    /**
     * Función que muestra el error del servidor en modal
     * @param value Mensaje de error.
     *              Por defecto se mostrará el mensaje 'No se pudo ejecutar la operación'.
     */

    hasServerError(value?: string) {
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: value || 'No se pudo ejecutar la operación',
            footer: ''
        })
    }

    successNotification(value?: string) {
        Swal.fire({
            icon: 'success',
            title: value === undefined ? 'Operación realizada con exito' : value,
            toast: true,
            // position: 'bottom-start',
            position: 'top',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });
    }

    hasValidationError(value?: string) {
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: value || 'Error al validar la información',
            footer: ''
        })
    }

    hasPermissionError(value?: string) {
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: value || 'No cuenta con permisos para acceder a este recurso.',
            footer: ''
        })
    }

    hasNotFoundError(value?: string) {
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: value || 'Recurso no encontrado.',
            footer: ''
        })
    }

    hasTimeoutError(value?: string) {
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: value || 'Tiempo de respuesta del servidor excedido.',
            footer: ''
        })
    }
}