import { Injectable } from '@angular/core';

declare var $: any;
declare var require: any;

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  pendingRequests = 0;
  showLoading: boolean;

  constructor() { }

  turnOnModal() {
    this.pendingRequests++;
    if (!this.showLoading) {
      this.showLoading = true;
      $("#loaderSpinner").show();
    }
    this.showLoading = true;
  }

  turnOffModal() {
    this.pendingRequests--;
    if (this.pendingRequests <= 0) {
      if (this.showLoading) {
        $("#loaderSpinner").hide();
      }
      this.showLoading = false;
    }

  }
}
