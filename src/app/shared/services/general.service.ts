import { Injectable } from '@angular/core';
import { General } from '../model/general';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  private _general: General;

  public get datos(): General {
    if (this._general != null) {
      return this._general;
    } else if (this._general == null && localStorage.getItem('userDetail') != null) {
      this._general = JSON.parse(localStorage.getItem('userDetail')) as General;
      return this._general;
    }
    return new General(null, null,null, null, null);
  }

  public clearDatos(){
    this._general = null;
  }
}