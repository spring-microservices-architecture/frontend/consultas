import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { apiPath } from 'src/environments/environment';
import { GeneralService } from './general.service';
import { Response } from '../model/response';

declare let require: any;
let ipify = require('ipify2');
declare const $: any;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private url = `${apiPath}/msauth/auth`;

  private _user: User;
  private _token: string;

  constructor(private httpClient: HttpClient,
    private generalService: GeneralService) { }

  public get user(): User {
    if (this._user != null) {
      return this._user;
    } else if (this._user == null && localStorage.getItem('user') != null) {
      this._user = JSON.parse(localStorage.getItem('user')) as User;
      return this._user;
    }
    return new User(null, null, '', '', '', '', '', '', '', null, null, null);
  }

  public get token(): string {
    if (this._token != null) {
      return this._token;
    } else if (this._token == null && localStorage.getItem('token') != null) {
      this._token = localStorage.getItem('token');
      return this._token;
    }
    return null;
  }

  login(user: User): Observable<any> {
    let data = JSON.stringify({ "usuario": user.username, "clave": user.password })
    return this.httpClient.post<any>(this.url+'/login', data, {
			headers: new HttpHeaders({
        'x-client-ip': user.ipAddress,
        'Content-Type': 'application/json',
				'Accept': 'application/json'
      }),
      observe: 'response'
    });
  }

  saveUser(accessToken: string, ipAddress: string): void {
    let payload = this.getPayloadToken(accessToken);
    this._user = new User(null, null, '', '', '', '', '', '', '', null, null, null);
    this._user.username = payload.user.username;
    this._user.nombres = payload.user.nombres;
    this._user.paterno = payload.user.paterno;
    this._user.materno = payload.user.materno;
    this._user.correo = payload.user.correo;
    this._user.celular = payload.user.celular;
    this._user.passwordChanged = payload.user.passwordChanged;
    //this._user.roles = payload.user.perfil;
    this._user.ipAddress = ipAddress;
    this._user.authorities = payload.authorities;

    localStorage.setItem("user", JSON.stringify(this._user));
  }

  saveToken(accessToken: string): void {
    this._token = accessToken;
    localStorage.setItem('token', accessToken);
  }

  getPayloadToken(accessToken: string): any {
    if (accessToken != null) {
      return JSON.parse(this.b64DecodeUnicode(accessToken.split(".")[1]));
    }
    return null;
  }

  b64DecodeUnicode(str) {
    return decodeURIComponent(atob(str).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
  }

  isAuthenticated(): boolean {
    let payload = this.getPayloadToken(this.token);
    if (payload != null && payload.user.username && payload.user.username.length > 0) {
      return true;
    }
    return false;
  }

  logout(): void {
    this._token = null;
    this._user = null;
    this.generalService.clearDatos();
    localStorage.clear();
    /*
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    */
  }
/*
  recoveryPassword(object: RecuperarClave): Observable<Response<any>> {//solicitar cambio de clave
    return this.httpClient.post<Response<any>>(`${this.url}/cambioclave`, object);
  }

  changePassword(object: RecuperarClave): Observable<Response<any>> {
    return this.httpClient.post<Response<any>>(`${this.url}/cambioclave/new-password`, object);
  }

  hasRole(role: string): boolean {
    if (this.user.roles.includes(role)) { // Includes: Valida si existe algun elemento dentro del arreglo
      return true;
    }
    return false;
  }
*/
  hasAuthority(authority: string): boolean {
    if (this.user.authorities.includes(authority)) {
      return true;
    }
    return false;
  }

  getFullName(): string {
    return this.user.nombres + ' ' + this.user.paterno + ' ' + this.user.materno;
  }

}