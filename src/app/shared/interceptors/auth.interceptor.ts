import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import Swal from 'sweetalert2';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { SweetAlertService } from 'src/app/shared/services/sweet-alert.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor{
    
    constructor(private authService: AuthService,
        private router: Router,
        private sweetAlertService: SweetAlertService) {

    }

    intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {

    return next.handle(req).pipe(
      catchError(e => {
        if (e.status == 401) {
          if (this.authService.isAuthenticated()) {
            this.authService.logout();
          }
          Swal.fire('Sesión expirada', `Su sesión ha caducado, por favor vuelva a ingresar al sistema.`, 'warning');
          this.router.navigate(['/login']);
        } else if (e.status == 403) {
          Swal.fire('Acceso denegado', `Hola ${this.authService.user.username} no tienes acceso a este recurso.`, 'warning');
          this.router.navigate(['/gps']);
        } else if (e.status == 0){
          this.sweetAlertService.hasServerError();
          //this.router.navigate(['/login']);
        }

        return throwError(e);

      })
    );

  }
  
}
