import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '../shared/model/user';
import { AuthService } from '../shared/services/auth.service';
import { SweetAlertService } from 'src/app/shared/services/sweet-alert.service';
import { Router, ActivatedRoute } from '@angular/router';

declare let require: any;
let ipify = require('ipify2');
declare const $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  isLogout: boolean;
  isInvalid: boolean;
  returnUrl: string = '/';
  user: User = new User(null, null, '', '', '', '', '', '', '', null, null, null);
  ipAddress: string = "0.0.0.0";
  RECAPTCHA_SITE_KEY: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private authService: AuthService,
    private swal: SweetAlertService
  ) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required, Validators.minLength(4)]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  onSubmit() {
    if (this.loginForm.invalid) return;

    this.user = Object.assign({}, this.loginForm.value);
    this.user.ipAddress = this.ipAddress;
console.log("user", this.user)
    this.authService.login(this.user).subscribe(response => {
      let autTokenHeader = response.headers.get('Authorization');
      this.authService.saveUser(autTokenHeader.split(" ")[1], this.ipAddress);
      this.authService.saveToken(autTokenHeader.split(" ")[1]);
      this.router.navigate(['/dashboard']);
    }, err => {
      if (err.status == 401) {
        this.swal.hasNotFoundError('Usuario o contraseña incorrecta');
      }
    });
  }
}